#pragma once

namespace engine {
    class Timer {
        public:
            Timer(int fps);
            ~Timer();

            int fps;
            int startTicks;
            bool isRunning;

            void start();
            void sleep();
            void update();
            int getTicks();
    };
}  // namespace engine
