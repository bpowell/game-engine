#pragma once

#include <SDL2/SDL.h>

#ifdef __APPLE__
#include <SDL2_image/SDL_image.h>
#else
#include <SDL2/SDL_image.h>
#endif

#include <string>
#include <functional>
#include <vector>
#include <sstream>
#include <fstream>

#include "dispatcher.h"
#include "sdl.h"

namespace engine {
    struct TileProperties {
        int number;
        std::function<void(SDL_Event*)> clickCallback;
        bool isClickable;
        bool isPassable;
        bool isReplaceable;
    };

    class Map : public EventDispatcher {
        public:
            Map(std::string filename, std::string spritesheetname, SDL_Renderer_Ptr renderer, SDL_Point frameSize);
            virtual ~Map();

            void render();
            int getTileByLocation(SDL_Rect location);
            int getTileByLocation(SDL_Point location);
            void dispatchEvent(SDL_Event *event) override;
            void replaceTile(SDL_Point location, int tile);

            std::vector<std::vector<int>> data;
            SDL_Renderer_Ptr renderer;
            SDL_Point frameSize;
            SDL_Texture_Ptr image;
            std::vector<TileProperties> tileProperties;
    };
}
