#pragma once

#include <SDL2/SDL.h>

typedef std::shared_ptr<SDL_Texture> SDL_Texture_Ptr;
typedef std::shared_ptr<SDL_Renderer> SDL_Renderer_Ptr;
typedef std::shared_ptr<SDL_Window> SDL_Window_Ptr;
