#pragma once

#include "timer.h"
#include "engine.h"
#include "ui.h"

namespace engine {
    class Stage {
        public:
            Stage(init_desc *desc);
            ~Stage();

            static Stage *instance;

            SDL_Renderer_Ptr renderer;
            std::list<Sprite_ptr> sprites;
            SDL_Rect camera;
            Sprite_ptr following;
            std::unique_ptr<init_desc> desc;
            Map_ptr map;
            Timer timer;
            UI *ui;

            void handleEvent(SDL_Event e);
            void render();
            void cameraFollows(Sprite_ptr f);
            void updateCamera();
            void update();
            void setMap(Map_ptr m);
            void setUI(UI *u);
    };

    Stage *getStage();
}
