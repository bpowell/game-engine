#pragma once

#include <SDL2/SDL.h>

#ifdef __APPLE__
#include <SDL2_image/SDL_image.h>
#else
#include <SDL2/SDL_image.h>
#endif

#include <string>
#include <list>
#include <functional>
#include <memory>
#include <vector>
#include <sstream>
#include <fstream>
#include <cmath>

#include "sdl.h"
#include "sprite.h"

namespace engine {
    class Hero : public Sprite {
        public:
            Hero(std::string filename, SDL_Renderer_Ptr renderer, SDL_Rect location);
            ~Hero();

            SDL_Point nextLocation;

            void update() override;
            void moveTo(SDL_Point next);
    };
}
