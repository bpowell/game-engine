#pragma once

#include "dispatcher.h"

namespace engine {
    class UI : public EventDispatcher {
        public:
            UI();
            ~UI();

            void dispatchEvent(SDL_Event *event) override;
    };
}
