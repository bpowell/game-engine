#pragma once

#include <SDL2/SDL.h>

#ifdef __APPLE__
#include <SDL2_image/SDL_image.h>
#else
#include <SDL2/SDL_image.h>
#endif

#include <string>

#include "sprite.h"
#include "sdl.h"

namespace engine {
    class SpriteSheet : public Sprite {
        public:
            SpriteSheet(std::string filename, SDL_Renderer_Ptr renderer, SDL_Rect location, SDL_Point frame, SDL_Point frameSize);
            ~SpriteSheet();

            SDL_Point frame;
            SDL_Point frameSize;

            void render();
    };
}
