#pragma once

#include <SDL2/SDL.h>

#ifdef __APPLE__
#include <SDL2_image/SDL_image.h>
#else
#include <SDL2/SDL_image.h>
#endif

#include <string>
#include <list>
#include <functional>
#include <memory>
#include <vector>
#include <sstream>
#include <fstream>

#include "sdl.h"
#include "dispatcher.h"
#include "map.h"
#include "sprite.h"
#include "hero.h"
#include "spritesheet.h"
#include "ui.h"

namespace engine {
    struct init_desc {
        int screen_width;
        int screen_height;
        int fps;

        std::string title;

        SDL_Point tile_size;
    };

    int init(init_desc *desc);
    SDL_Window_Ptr getWindow();
    bool update();
    bool handle_event(SDL_Event &e);

    typedef std::shared_ptr<Sprite> Sprite_ptr;
    typedef std::shared_ptr<Hero> Hero_ptr;
    typedef std::shared_ptr<SpriteSheet> SpriteSheet_ptr;
    typedef std::shared_ptr<Map> Map_ptr;

}  // namespace engine
