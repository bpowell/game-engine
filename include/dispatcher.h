#pragma once

#include <SDL2/SDL.h>
#include <list>
#include <functional>
#include <memory>

namespace engine {
    struct listener {
        SDL_EventType type;
        std::function<void(SDL_Event*)> callback;
    };

    typedef std::shared_ptr<listener> listener_ptr;

    class EventDispatcher {
        public:
            EventDispatcher();
            ~EventDispatcher();

            virtual void dispatchEvent(SDL_Event *event);
            void dispatchEvent(SDL_Event &event) { dispatchEvent(&event); }
            void addEventListener(SDL_EventType t, std::function<void(SDL_Event*)> cb);
            std::function<void(SDL_Event*)> getCallback(SDL_Event &e);

            std::list<listener_ptr> listeners;
    };
}
