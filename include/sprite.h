#pragma once

#include <SDL2/SDL.h>

#ifdef __APPLE__
#include <SDL2_image/SDL_image.h>
#else
#include <SDL2/SDL_image.h>
#endif

#include <string>
#include <list>
#include <functional>
#include <memory>
#include <vector>
#include <sstream>
#include <fstream>

#include "dispatcher.h"
#include "sdl.h"

namespace engine {
    class Sprite : public EventDispatcher {
        public:
            Sprite(std::string filename, SDL_Renderer_Ptr renderer, SDL_Rect location);
            virtual ~Sprite();

            SDL_Renderer_Ptr renderer;
            SDL_Rect location;
            SDL_Texture_Ptr image;
            bool isClicked;

            void dispatchEvent(SDL_Event *event) override;

            void setLocation(SDL_Rect l);
            virtual void render();
            virtual void update();
    };
}
