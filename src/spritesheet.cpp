#include "spritesheet.h"
#include "stage.h"

namespace engine {
    SpriteSheet::SpriteSheet(std::string filename, SDL_Renderer_Ptr renderer, SDL_Rect location, SDL_Point frame, SDL_Point frameSize) : Sprite(filename, renderer, location), frame(frame), frameSize(frameSize) {
    }

    SpriteSheet::~SpriteSheet(){}

    void SpriteSheet::render() {
        auto camera = getStage()->camera;
        SDL_Rect tmp = {location.x - camera.x, location.y - camera.y, location.w, location.h};

        SDL_Rect clip;
        clip.x = frame.x * frameSize.x;
        clip.y = frame.y * frameSize.y;
        clip.w = frameSize.x;
        clip.h = frameSize.y;

        SDL_RenderCopy(renderer.get(), image.get(), &clip, &tmp);
    }
}
