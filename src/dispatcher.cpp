#include <iostream>

#include "dispatcher.h"
#include "stage.h"

namespace engine {
    EventDispatcher::EventDispatcher() {
    }

    EventDispatcher::~EventDispatcher() {
    }

    void EventDispatcher::dispatchEvent(SDL_Event *event) {
        std::cout << "event dispatched\n";
        getStage()->handleEvent(*event);
    }

    void EventDispatcher::addEventListener(SDL_EventType t, std::function<void(SDL_Event*)> cb) {
        listener_ptr l = std::make_shared<listener>();
        l->type = t;
        l->callback = cb;

        listeners.push_front(l);
    }

    std::function<void(SDL_Event*)> EventDispatcher::getCallback(SDL_Event &e) {
        SDL_EventType type = (SDL_EventType) e.type;
        for(auto l : listeners) {
            if(l->type == type) {
                return l->callback;
            }
        }

        return NULL;
    }
}
