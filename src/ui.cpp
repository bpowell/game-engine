#include <iostream>
#include "ui.h"

namespace engine {
    UI::UI() {}
    UI::~UI() {}

    void UI::dispatchEvent(SDL_Event *event) {
        switch(event->type) {
            case SDL_KEYDOWN:
                switch (event->key.keysym.sym) {
                    case SDLK_i:
                        std::cout << "i";
                }
        }
    }
}
