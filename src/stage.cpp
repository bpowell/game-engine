#include <iostream>

#include "stage.h"
#include "engine.h"

namespace engine {
    Stage::Stage(init_desc *desc) : desc(std::unique_ptr<init_desc>(desc)), timer(Timer(desc->fps)) {
        renderer = SDL_Renderer_Ptr(SDL_CreateRenderer(getWindow().get(), -1, SDL_RENDERER_ACCELERATED), SDL_DestroyRenderer);
        if(renderer.get() == NULL) {
            std::cout << "RENDERER IS NULL\n" << SDL_GetError();
        }

        timer.start();
    }

    Stage::~Stage() {
    }

    void Stage::handleEvent(SDL_Event e) {
        for(auto s : sprites) {
            s->dispatchEvent(&e);
        }
        map->dispatchEvent(&e);
        ui->dispatchEvent(&e);
    }

    void Stage::render() {
        SDL_RenderClear(renderer.get());
        map->render();
        for(auto s : sprites) {
            s->render();
        }
        SDL_RenderPresent(renderer.get());
    }

    void Stage::cameraFollows(Sprite_ptr f) {
        following = f;
        updateCamera();
    }

    void Stage::update() {
        for(auto s : sprites) {
            s->update();
        }

        updateCamera();
    }

    void Stage::updateCamera() {
        camera.x = (following->location.x + desc->tile_size.x) - desc->screen_width / 2;
        camera.y = (following->location.y + desc->tile_size.y) - desc->screen_height / 2;

        if(camera.x < 0) {
            camera.x = 0;
        }

        if(camera.y < 0) {
            camera.y = 0;
        }

        if(camera.x > desc->screen_width + camera.w) {
            camera.x = desc->screen_width - camera.w;
        }

        if(camera.y > desc->screen_height + camera.h) {
            camera.y = desc->screen_height - camera.h;
        }

        camera.w = desc->screen_width + camera.x;
        camera.h = desc->screen_height + camera.y;
    }

    void Stage::setMap(Map_ptr m) {
        map = m;
    }

    void Stage::setUI(UI *u) {
        ui = u;
    }

    Stage *Stage::instance;

    Stage *getStage() {
        return Stage::instance;
    }
}
