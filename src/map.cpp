#include "stage.h"
#include "map.h"

namespace engine {
    Map::Map(std::string filename, std::string spritesheetname, SDL_Renderer_Ptr renderer, SDL_Point frameSize) : renderer(renderer), frameSize(frameSize) {
        std::ifstream infile(filename);
        for(std::string line; std::getline(infile, line); ) {
            std::istringstream temp(line);
            std::vector<int> inner;

            for (std::string ch; std::getline(temp, ch, ',');) {
                inner.push_back(std::stoi(ch, nullptr));
            }

            data.push_back(inner);
        }

        image = SDL_Texture_Ptr(IMG_LoadTexture(renderer.get(), spritesheetname.c_str()), SDL_DestroyTexture);
    }

    Map::~Map() {}

    void Map::render() {
        int i = 0, j = 0;
        for(auto y : data) {
            for(auto z : y) {
                auto camera = getStage()->camera;

                SDL_Rect location;
                location.x = j * frameSize.x - camera.x;
                location.y = i * frameSize.y - camera.y;
                location.w = frameSize.x;
                location.h = frameSize.y;

                if(!SDL_HasIntersection(&location, &camera)) {
                    continue;
                }

                SDL_Rect clip;
                clip.x = z * frameSize.x;
                clip.y = 0;
                clip.w = frameSize.x;
                clip.h = frameSize.y;

                SDL_RenderCopy(renderer.get(), image.get(), &clip, &location);
                j++;
            }
            j = 0;
            i++;
        }
    }

    void Map::dispatchEvent(SDL_Event *event) {
        auto callback = getCallback(*event);
        if(callback != NULL) {
            callback(event);
        }
    }

    int Map::getTileByLocation(SDL_Rect location) {
        SDL_Point l;
        l.x = location.x;
        l.y = location.y;
        return getTileByLocation(l);
    }

    int Map::getTileByLocation(SDL_Point location) {
        int y = location.x / frameSize.x;
        int x = location.y / frameSize.y;

        if(x>(int)data.size()) {
            return -1;
        }

        if(y>(int)data[x].size()) {
            return -1;
        }

        return data[x][y];
    }

    void Map::replaceTile(SDL_Point location, int tile) {
        if(location.y < (int)data.size()) {
            if(location.x < (int)data[location.y].size()) {
                data[location.y][location.x] = tile;
            }
        }
    }
}
