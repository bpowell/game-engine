#include <iostream>

#include "stage.h"
#include "engine.h"

namespace engine {
    SDL_Window_Ptr window;
    EventDispatcher *dispatcher;

    int init(init_desc *desc) {
        if(SDL_Init(SDL_INIT_VIDEO) < 0) {
            std::cout << SDL_GetError();
            return 1;
        }

        window = SDL_Window_Ptr(SDL_CreateWindow(desc->title.c_str(), SDL_WINDOWPOS_UNDEFINED,
                SDL_WINDOWPOS_UNDEFINED, desc->screen_width, desc->screen_height,
                SDL_WINDOW_SHOWN), SDL_DestroyWindow);
        if(window.get() == NULL) {
            std::cout << "WINDOW ERROR!\n" << SDL_GetError();
        }

        if(!Stage::instance) {
            Stage::instance = new Stage(desc);
        }

        if(!dispatcher) {
            dispatcher = new EventDispatcher;
        }

        return 0;
    }

    SDL_Window_Ptr getWindow() {
        return window;
    }

    bool update() {
        getStage()->timer.update();
        bool done;
        SDL_Event e;

        while(SDL_PollEvent(&e)) {
            done = handle_event(e);
        }

        getStage()->update();
        getStage()->render();
        getStage()->timer.sleep();

        return done;
    }

    bool handle_event(SDL_Event &e) {
        dispatcher->dispatchEvent(e);

        switch(e.type) {
            case SDL_QUIT:
                return true;
        }

        return false;
    }

}  // namespace engine
