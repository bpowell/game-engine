#include "sprite.h"
#include "stage.h"

namespace engine {
    Sprite::Sprite(std::string filename, SDL_Renderer_Ptr renderer, SDL_Rect location) : renderer(renderer), location(location), isClicked(false) {
        image = SDL_Texture_Ptr(IMG_LoadTexture(renderer.get(), filename.c_str()), SDL_DestroyTexture);
    }

    Sprite::~Sprite() {}

    void Sprite::dispatchEvent(SDL_Event *event) {
        switch(event->type) {
            case SDL_MOUSEBUTTONDOWN:
                SDL_Rect mouse;
                mouse.w = mouse.h = 1;
                SDL_GetMouseState(&mouse.x, &mouse.y);
                isClicked = SDL_HasIntersection(&location, &mouse);
                break;
            case SDL_MOUSEBUTTONUP:
                isClicked = false;
                break;
        }

        auto callback = getCallback(*event);
        if(callback != NULL) {
            callback(event);
        }
    }

    void Sprite::setLocation(SDL_Rect l) {
        location = l;
    }

    void Sprite::update() {}

    void Sprite::render() {
        auto camera = getStage()->camera;
        SDL_Rect tmp = {location.x - camera.x, location.y - camera.y, location.w, location.h};
        SDL_RenderCopy(renderer.get(), image.get(), NULL, &tmp);
    }
}
