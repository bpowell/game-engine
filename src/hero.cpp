#include "stage.h"
#include "hero.h"

namespace engine {
    Hero::Hero(std::string filename, SDL_Renderer_Ptr renderer, SDL_Rect location) : Sprite(filename, renderer, location), nextLocation({-1,-1}) {
    }

    Hero::~Hero() {}

    void Hero::update() {
        if (nextLocation.x == -1 && nextLocation.y == -1) {
            // do nothing, we have not moved yet
            return;
        }

        if (nextLocation.x == location.x && nextLocation.y == location.y) {
            // do nothing, we are at the location
            nextLocation.x = nextLocation.y = -1;
            return;
        }

        auto loc = location;

        if (nextLocation.x > location.x) {
            location.x += location.h;
        } else if (nextLocation.x < location.x) {
            location.x -= location.h;
        }

        if (nextLocation.y > location.y) {
            location.y += location.w;
        } else if (nextLocation.y < location.y) {
            location.y -= location.w;
        }

        auto tile = getStage()->map->getTileByLocation(location);
        if(tile < (int)getStage()->map->tileProperties.size()) {
            auto props = getStage()->map->tileProperties[tile];
            if(!props.isPassable) {
                location = loc;
            }
        }
    }

    void Hero::moveTo(SDL_Point next) {
        int x = int(std::floor(next.x / location.w)) * location.w + getStage()->camera.x;
        int y = int(std::floor(next.y / location.h)) * location.h + getStage()->camera.y;
        nextLocation = {x,y};
    }
}
