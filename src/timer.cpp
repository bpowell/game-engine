#include "timer.h"
#include "engine.h"

namespace engine {
    Timer::Timer(int fps) : fps(fps), startTicks(0), isRunning(true) {}
    Timer::~Timer() {}

    void Timer::start() {
        isRunning = true;
    }

    void Timer::sleep() {
        if (getTicks() < (1000 / fps)) {
            SDL_Delay((1000 / fps) - getTicks());
        }
    }

    void Timer::update() {
        startTicks = SDL_GetTicks();
    }

    int Timer::getTicks() {
        return SDL_GetTicks() - startTicks;
    }
}
