#include <iostream>

#include <engine.h>
#include <stage.h>

int main() {
    engine::init_desc desc;
    desc.screen_width = 800;
    desc.screen_height = 600;
    desc.fps = 60;
    desc.title = "My App";
    desc.tile_size = {32, 32};

    engine::init(&desc);

    engine::Sprite_ptr s = engine::Sprite_ptr(new engine::Sprite(
        "toon.png", engine::getStage()->renderer, {0, 0, 16, 16}));

    auto cb = [s](SDL_Event *e) {
        std::cout << "HELLO WORLD\n";
        std::cout << s->isClicked << std::endl;
    };

    s->addEventListener(SDL_MOUSEBUTTONDOWN, cb);
    engine::getStage()->sprites.push_front(s);

    engine::SpriteSheet_ptr ss = engine::SpriteSheet_ptr(
        new engine::SpriteSheet("tiles.png", engine::getStage()->renderer,
                                {100, 100, 180, 180}, {0, 0}, {80, 80}));
    engine::getStage()->sprites.push_front(ss);

    engine::Hero_ptr hero = engine::Hero_ptr(new engine::Hero(
        "toon.png", engine::getStage()->renderer, {50, 50, 16, 16}));
    engine::getStage()->sprites.push_front(hero);
    auto herocb = [hero](SDL_Event *e) {
        std::cout << "HERO!\n";
        if (e->type == SDL_MOUSEBUTTONDOWN) {
            SDL_Point mouse;
            SDL_GetMouseState(&mouse.x, &mouse.y);
            hero->moveTo(mouse);
            std::cout << hero->location.x << ":" << hero->location.y
                      << std::endl;
        }
    };

    hero->addEventListener(SDL_MOUSEBUTTONDOWN, herocb);

    engine::getStage()->cameraFollows(hero);

    engine::Map_ptr m = engine::Map_ptr(new engine::Map(
        "map.txt", "tiles.png", engine::getStage()->renderer, {80, 80}));
    engine::getStage()->setMap(m);

    auto wcb = [m](SDL_Event *e) {
        if (e->type == SDL_MOUSEBUTTONDOWN) {
            std::cout << "fuckk\n";
            SDL_Point mouse;
            SDL_GetMouseState(&mouse.x, &mouse.y);
            mouse.x = int(std::floor(mouse.x / m->frameSize.x));
            mouse.y = int(std::floor(mouse.y / m->frameSize.y));
            m->replaceTile(mouse, 2);
        }
    };
    engine::TileProperties waterprops;
    waterprops.number = 0;
    waterprops.isPassable = false;
    waterprops.isReplaceable = true;
    waterprops.isClickable = true;
    waterprops.clickCallback = wcb;
    m->tileProperties.push_back(waterprops);

    auto tpcb = [](SDL_Event *e) { std::cout << "clicked\n"; };
    engine::TileProperties tp;
    tp.clickCallback = tpcb;
    tp.number = 1;
    tp.isClickable = true;
    tp.isPassable = true;
    m->tileProperties.push_back(tp);

    auto mcb = [m](SDL_Event *e) {
        if (e->type == SDL_MOUSEBUTTONDOWN) {
            SDL_Point mouse;
            SDL_GetMouseState(&mouse.x, &mouse.y);
            auto tile = m->getTileByLocation(mouse);
            m->tileProperties[tile].clickCallback(e);
        }
    };
    m->addEventListener(SDL_MOUSEBUTTONDOWN, mcb);

    engine::UI *ui = new engine::UI;
    engine::getStage()->setUI(ui);

    bool done = engine::update();
    while (!done) {
        done = engine::update();
    }

    return 0;
}
